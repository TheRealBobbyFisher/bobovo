﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    class CompoundPart: PartInstance
    {
        public List<PartInstance> Children { get; set; }
        public CompoundPart(DataRow row, PartPrototype prototype) :base(row, prototype)
        {
            Children = new List<PartInstance>();
        }
    }

}
