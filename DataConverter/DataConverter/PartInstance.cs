﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    [DebuggerDisplay("{ExternalID}")]
    class PartInstance: VirtualPart
    {
        public PartPrototype Prototype { get; set; }
        public PartInstance(DataRow row, PartPrototype prototype):base(row)
        {
            this.Prototype = prototype;
        }
    }
}
