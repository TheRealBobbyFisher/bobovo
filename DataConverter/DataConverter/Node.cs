﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    class Node
    {
        public string PNGUID { get; set; }

        public List<Node> Children { get; }

        public Node Parent { get; set; }

        public List<DataRow> Records { get; }

        public Node(string pnguid, DataRow row)
        {
            this.PNGUID = pnguid;
            this.Children = new List<Node>();
            this.Records = new List<DataRow>();
            this.Records.Add(row);
        }

        public Node(string pNGUID, List<DataRow> records)
        {
            PNGUID = pNGUID;
            Records = records;
        }

        public void AddRecord(DataRow record)
        {
            this.Records.Add(record);
        }

        public override string ToString()
        {
            return "Node with PNGUID:" + PNGUID;
        }
    }
}
