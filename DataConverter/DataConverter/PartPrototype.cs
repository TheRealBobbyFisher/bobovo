﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    [DebuggerDisplay("{Description}")]
    class PartPrototype: VirtualPart
    {
        public PartPrototype(DataRow row):base(row)
        {            
        }
    }
}
