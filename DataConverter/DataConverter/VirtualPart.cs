﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    class VirtualPart
    {
        public DataRow PrimitiveDataRow { get; set; }
        public string ExternalID { get; set; }
        public string Description { get; set; }
        public VirtualPart(DataRow row)
        {
            PrimitiveDataRow = row;
            Description = $"AENNR: {row["AENNR"]}, MATNR: {row["MATNR"]}, MAKTX: {row["MAKTX"]}, CS: {row["LANGUAGE2"]}";
            ExternalID = $"{row["AENNR"]}{row["PNGUID"]}";
        }
        
    }
}
