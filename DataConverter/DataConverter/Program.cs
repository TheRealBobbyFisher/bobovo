﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConverter
{
    class Program
    {

        static void Main(string[] args)
        {
            var data = new DataTable();
            data.ReadXmlSchema("schema.xml");
            data.ReadXml("data.xml");
            var root = ParseTable(data);
            var MasterParts = CreateDataStructure(root);
            Console.WriteLine("x");
            List<VirtualPart> flatParts = CreateFlatDataStructure(MasterParts[0]);
            int i = 0;
            foreach (var flatpart in flatParts)
            {
                Console.WriteLine(i++ + ": " + flatpart.Description);
            }


        }
        private static List<VirtualPart> CreateFlatDataStructure(VirtualPart part)
        {
            List<VirtualPart> virtuals = new List<VirtualPart>();
            if (part is CompoundPart)
            {
                virtuals.Add(part);
                foreach (var child in ((CompoundPart)part).Children)
                {
                    virtuals.AddRange(CreateFlatDataStructure(child));
                }
            }
            else
            {
                virtuals.Add(part);
                virtuals.Add(((PartInstance)part).Prototype);
            }
            return virtuals;
        }
        private static List<VirtualPart> CreateFlatDataStructure2(VirtualPart part)
        {
            List<VirtualPart> kappaList = new List<VirtualPart>();
            InnerRecursion(part);

            void InnerRecursion(VirtualPart kappa)
            {
                if (part is CompoundPart)
                {
                    foreach (var child in ((CompoundPart)part).Children)
                    {
                        kappaList.Add(child);
                        InnerRecursion(child);
                    }
                }
                else
                {
                    kappaList.Add(part);
                    kappaList.Add(((PartInstance)part).Prototype);
                }
                //return kappaList;
            }

            return kappaList;
        }

        private static List<PartInstance> CreateDataStructure(Node node)
        {

            List<PartInstance> listOfParts = new List<PartInstance>();
            Dictionary<string, PartPrototype> existingPrototypes = new Dictionary<string, PartPrototype>();

            if (node.Children.Count == 0)
            {
                return ProcessLeaf(node, listOfParts, existingPrototypes);
            }
            else
            {
                return ProccesParentNode(node, listOfParts, existingPrototypes);
            }
        }

        private static List<PartInstance> ProccesParentNode(Node node, List<PartInstance> listOfParts, Dictionary<string, PartPrototype> existingPrototypes)
        {
            CompoundPart compoundPart = new CompoundPart(GetFirstImportedRecord(node.Records), null);
            foreach (DataRow record in node.Records)
            {

                if (Convert.ToInt32(record["IMPORT"]) == 1)
                {
                    string prototypeID = GetPrototypeID(existingPrototypes, record, true);
                    compoundPart.Children.Add(new PartInstance(record, existingPrototypes[prototypeID]));
                }
            }
            foreach (var child in node.Children)
            {
                compoundPart.Children.AddRange(CreateDataStructure(child));
            }
            listOfParts.Add(compoundPart);
            return listOfParts;
        }
        private static List<PartInstance> ProcessLeaf(Node node, List<PartInstance> listOfParts, Dictionary<string, PartPrototype> existingPrototypes)
        {
            foreach (DataRow record in node.Records)
            {
                if (Convert.ToInt32(record["IMPORT"]) == 1)
                {
                    string prototypeID = GetPrototypeID(existingPrototypes, record, false);
                    listOfParts.Add(new PartInstance(record, existingPrototypes[prototypeID]));
                }

            }
            return listOfParts;
        }
        private static string GetPrototypeID(Dictionary<string, PartPrototype> existingPrototypes, DataRow record, bool isBauteilGruppe)
        {
            string prototypeID = ((string)record["MATNR"]).Substring(3, 6);
            if (!existingPrototypes.ContainsKey(prototypeID) && isBauteilGruppe)
            {
                existingPrototypes.Add(prototypeID, new BauteilGruppe(record));
            }
            else if (!existingPrototypes.ContainsKey(prototypeID) && !isBauteilGruppe)
            {
                existingPrototypes.Add(prototypeID, new PartPrototype(record));
            }
            return prototypeID;
        }
        private static DataRow GetFirstImportedRecord(List<DataRow> list)
        {
            foreach (var record in list)
            {
                if (Convert.ToInt32(record["IMPORT"]) == 1)
                {
                    return record;
                }
            }
            return list[0];
        }

        private static Node ParseTable(DataTable data)
        {
            //var nodes = new Dictionary<string, Node>();

            //foreach (DataRow row in data.Rows) {
            //    if (nodes.ContainsKey((string)row["PNGUID"])) {
            //        nodes[(string)row["PNGUID"]].AddRecord(row);
            //    }
            //    else {
            //        nodes.Add((string)row["PNGUID"], new Node((string)row["PNGUID"], row));
            //    }
            //}

            var nodes = data.AsEnumerable()
                .GroupBy(row => (string)row["PNGUID"])
                .ToDictionary(group => group.Key, group => new Node(group.Key, group.ToList()));


            foreach (var node in nodes)
            {
                var parentGuid = (string)node.Value.Records[0]["GUID1"];

                if (nodes.TryGetValue(parentGuid, out var parent))
                {
                    parent.Children.Add(node.Value);
                    node.Value.Parent = parent;
                }
            }
            return nodes.Values.SingleOrDefault(node => node.Parent == null);
        }
    }
}
